# Toastmasters Tools
Version 2.0  
by Eric Boring

A small set of tools for Toastmasters members. This project currently provides tools for:
- Ah Counter
- Timer
- Word of The Day
- A Random Participant Picker (useful for Table Topics)

Participants added to the application are saved in the web browser's local storage so
that adding all of your club's members is not require every meeting. The states of the Picker
tab, WOTD tab, Timer tab, and Ah Counter tab are saved so that every time you open the page it 
looks the same as last time. 

The application is designed to be usable as a downloaded, stand-alone file. Any user can
download the HTML document and open it in their web browser. It does require internet
access as it relies heavily on jQuery, which is not included in the file. It can also be
hosted on any web server for easier access. It is currently being hosted on GitLab Pages
and can be used for free here: https://zerob414.gitlab.io/toastmasters-tools/Toastmasters%20Tools.html

---

## Changes
__7 Feb 2021, Ver 2.0:__ Added saving and restoring Ah Counter tab's state. All tabs are now saved and
restored.

__29 Jan 2021, Ver 1.9:__ Added saving and restoring of the Timer tab's state.

__24 Jan 2021, Ver 1.8:__ Added saving and restoring of the WOTD tab's state.

__14 Jan 2021, Ver 1.7:__ Added saving and restoring of the picker tab's state.

__2 Jan 2021, Ver 1.6:__ Added a "Help" tab with some brief info about each tool and URL fragments
to automatically switch to a specific tab. Also a few bug fixes, and a slight change in how participants
are identified under the hood to make adding future features easier. 

__29 Dec 2020, Ver 1.5:__ Added icons to the tabs and added _Dark Mode_.

__12 Dec 2020, Ver 1.4:__ Fixed a bug in which people marked as not present in the Participants tab
would still show up in the reports on the other tabs. Also ensured the picker doesn't pick absent
people, and added a copy/paste text box to the WOTD tab for easier sharing on Zoom chat.

---

This product is not affiliated with Toastmasters International in any way.
These tools are provided "as is" with no promise of support and no
guarantees of compatibility with any particular browsers. 